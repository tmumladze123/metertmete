package com.example.shemajamebelitertmeti.network

import com.example.shemajamebelitertmeti.model.messageItem
import kotlinx.coroutines.flow.Flow
import retrofit2.Response
import retrofit2.http.GET


interface getMessageApi {
    @GET("80d25aee-d9a6-4e9c-b1d1-80d2a7c979bf")
    suspend fun getMessages() : Response<ArrayList<messageItem>>
}