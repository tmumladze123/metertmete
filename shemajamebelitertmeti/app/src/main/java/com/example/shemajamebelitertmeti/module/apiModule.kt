package com.example.shemajamebelitertmeti.module

import com.example.shemajamebelitertmeti.network.getMessageApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object apiModule {
    @Provides
    fun provideBaseUrl() = "https://run.mocky.io/v3/"

    @Provides
    @Singleton
    fun provideRetrofitInstance(BASE_URL: String): getMessageApi =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(getMessageApi::class.java)
}