package com.example.shemajamebelitertmeti.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.shemajamebelitertmeti.model.messageItem
import com.example.shemajamebelitertmeti.network.Resource
import com.example.shemajamebelitertmeti.repository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class messageViewModel @Inject constructor(var repo: repository) : ViewModel() {
   private var _messageFlow = MutableStateFlow<ArrayList<messageItem>>(arrayListOf())
    val messageFlow :  MutableStateFlow<ArrayList<messageItem>> get() = _messageFlow
     fun connect() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                setMessages()
            }
        }
    }
    /*suspend fun setMessages()
    {
        _messageFlow.emit(repo.Messages()!!.value)

    }*/
    suspend fun setMessages()
    {


        _messageFlow.emit(repo.Messages().value.data!!)

    }

}


