package com.example.shemajamebelitertmeti.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.shemajamebelitertmeti.Extensions.setPhoto
import com.example.shemajamebelitertmeti.R
import com.example.shemajamebelitertmeti.databinding.MessageitemBinding
import com.example.shemajamebelitertmeti.model.messageItem

class messageAdapter( ) : RecyclerView.Adapter<messageAdapter.ViewHolder>() {
    var messages= mutableListOf<messageItem>()
    inner class ViewHolder(val binding: MessageitemBinding) : RecyclerView.ViewHolder(binding.root) {
        private lateinit var message: messageItem
        @SuppressLint("SetTextI18n")
        fun bind()
        {  message= messages[adapterPosition]
            binding.apply {
                messageAvatar.setPhoto(message.avatar)
                tvName.text=message.firstName
                tvLastName.text=message.lastName
                when(message.messageType.toString()) {
                  "voice" -> {
                      icon.setImageResource(R.drawable.ic_recorder)
                      icon.visibility= View.VISIBLE
                      tvLastMessage.text="Sent a voice message"
                  }
                  "attachment" -> {
                      icon.setImageResource(R.drawable.ic_attachment)
                      icon.visibility= View.VISIBLE
                      tvLastMessage.text="Sent an attachment"
                  }
                  else ->tvLastMessage.text=message.lastMessage
                }
                if(message.unreaMessage!!.toInt() > 0) {
                    tvUnreadMessage.text= message.unreaMessage.toString()
                    tvUnreadMessage.setBackgroundResource(R.drawable.border)
                }
                if(message.isTyping==true) {
                    tvDots.setImageResource(R.drawable.ic_group_14)
                    tvDots.visibility=View.VISIBLE
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(MessageitemBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }
    override fun getItemCount(): Int =messages.size
    fun setData(messageItems : MutableList<messageItem>) {
        messages.clear()
        messages.addAll(messageItems)
        notifyDataSetChanged()
    }
}