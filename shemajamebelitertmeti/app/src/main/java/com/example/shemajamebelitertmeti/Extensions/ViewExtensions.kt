package com.example.shemajamebelitertmeti.Extensions


import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide

fun View.setPhoto(url:String?)
{
    Glide.with(context).load(url).into(this as ImageView)
}