package com.example.shemajamebelitertmeti.fragments

import android.util.Log.d
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.shemajamebelitertmeti.adapter.messageAdapter
import com.example.shemajamebelitertmeti.databinding.FragmentMessageBinding
import com.example.shemajamebelitertmeti.viewModel.messageViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MessageFragment :  BaseFragment<FragmentMessageBinding>(FragmentMessageBinding::inflate) {

    private val messageModel: messageViewModel by viewModels()
    var adapter=messageAdapter()
    override fun start() {
        initAdapter()
        messageModel.connect()
        observers()
    }
    fun initAdapter(){
        binding.rvMessages.adapter=adapter
        binding.rvMessages.layoutManager= LinearLayoutManager(context)
    }
    fun observers(){
        viewLifecycleOwner.lifecycleScope.launch {
            messageModel.messageFlow.collect {
                adapter.setData(it.toMutableList())
                d("mess", it.toString())
            }
        }
    }
}



